# NUMBER26 Code challenge #

### What is this repository for? ###
This repository contains solution to the NUMBER26 Code challenge

### How do I get set up? ###

* It is required to have [Gradle](http://gradle.org/) & Java set up

### How to run the application? ###
* cd number26-code-challenge/
* gradle bootRun

### How to simulate REST requests easily? ###
[httpie](http://httpie.org) is the way to go (or any other tool such as cURL, Postman for Chrome)

Example usage:

* http PUT localhost:8080/transactionservice/transaction/10 amount=5000 type=cars
* http PUT localhost:8080/transactionservice/transaction/11 amount=10000 type=shopping parent_id=10
* http PUT localhost:8080/transactionservice/transaction/12 amount=9000 type=tech parent_id=11
* http localhost:8080/transactionservice/transactions/
* http localhost:8080/transactionservice/types/cars
* http localhost:8080/transactionservice/sum/10
* http localhost:8080/transactionservice/sum/11


### How to run tests? ###
* gradle test
* Application was also tested with [Apache Bench](https://httpd.apache.org/docs/2.2/programs/ab.html) in order to rule out concurrency issues and performance bottlenecks

Example usage:

* ab.exe -u tx11.json -T application/json -c 1000 -n 2000 localhost:8080/transactionservice/transaction/11
* ab.exe -u tx10.json -T application/json -c 1000 -n 2000 localhost:8080/transactionservice/transaction/10
* ab.exe -c 1000 -n 5000 localhost:8080/transactionservice/sum/10

### Who do I talk to? ###

* In case of any questions, feel free to reach out. I am more than open to any questions, suggestions, corrections.
* [Patrik Mihalčin](http://mihalc.in/)