package in.mihalc.number26;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Patrik Mihalčin
 * TransactionController is a RestController which handles REST GET and PUT requests.
 * This controller allows for adding new transactions, getting sum of amounts for particular transaction_id and get all transaction_ids with given transaction type
 * <p>
 * Spring Boot uses the Jackson JSON library to automatically marshal instances into JSON
 * Mapping definitions match API defined in Number 26 Code Challenge
 * In order to avoid concurrent issues, ConcurrentHashMap was chosen as appropriate thread-safe data structure where information is kept based on aggregation criteria
 * <p>
 * Assumptions made:
 * GET requests will be more frequent than PUT requests, this is the reason code does pre-processing in PUT requests
 * <p>
 * Asymptotic behavior
 * GET requests - depends on the complexity of ConcurrentHashMap
 * PUT requests - O(n) because code updates parent transactions with amounts
 */
@RestController
@RequestMapping("/transactionservice")
public class TransactionController {

    private Map<Long, Transaction> sumByTranId = new ConcurrentHashMap<>();
    private Map<String, Set<Long>> tranIdsByType = new ConcurrentHashMap<>();

    @RequestMapping(value = "/transaction/{transaction_id}", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.CREATED)
    public void addTransaction(@PathVariable Long transaction_id, @RequestBody Transaction incomingTx) {
        incomingTx.setTransaction_id(transaction_id);

        Set<Long> tranIds = tranIdsByType.get(incomingTx.getType());

        if (tranIds == null) {
            Set<Long> idSet = new HashSet<>();
            idSet.add(incomingTx.getTransaction_id());
            tranIdsByType.put(incomingTx.getType(), idSet);
        } else {
            tranIds.add(incomingTx.getTransaction_id());
        }

        reflectAmountInParents(incomingTx);
    }

    private void reflectAmountInParents(Transaction incomingTx) {
        if (sumByTranId.get(incomingTx.getTransaction_id()) == null) {
            sumByTranId.put(incomingTx.getTransaction_id(), incomingTx);
        }

        Transaction tx = incomingTx;
        while (tx.getParent_id() != null) {
            Transaction parentTx = sumByTranId.get(tx.getParent_id());
            if (parentTx == null) {
                break;
            }
            parentTx.setAmount(parentTx.getAmount() + incomingTx.getAmount());
            tx = parentTx;
        }
    }

    @RequestMapping(value = "/types/{type}", method = RequestMethod.GET)
    public Set<Long> types(@PathVariable String type) {
        Set<Long> tranIds = tranIdsByType.get(type);
        if (tranIds == null) {
            throw new TransactionNotFoundException("No transactions of type '" + type + "' were found");
        }
        return tranIds;
    }

    @RequestMapping(value = "/sum/{transaction_id}", method = RequestMethod.GET)
    public Double sum(@PathVariable Long transaction_id) {
        Transaction tx = sumByTranId.get(transaction_id);
        if (tx == null) {
            throw new TransactionNotFoundException("Transaction with transaction_id '" + transaction_id + "' was found");
        }
        return tx.getAmount();
    }

    public Map<Long, Transaction> getSumByTranId() {
        return sumByTranId;
    }

    public Map<String, Set<Long>> getTranIdsByType() {
        return tranIdsByType;
    }
}