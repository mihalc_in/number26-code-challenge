package in.mihalc.number26;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

/**
 * Created by Patrik Mihalčin
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class TransactionControllerTest {

    private static final Logger log = LoggerFactory.getLogger(TransactionControllerTest.class);

    RestTemplate restTemplate = new TestRestTemplate();

    @Autowired
    private TransactionController transactionController;

    @Before
    public void setUp() throws IOException {
        log.info("@Before setUp()...");

        transactionController.getSumByTranId().clear();
        transactionController.getTranIdsByType().clear();

        String requestBody =
                "{" +
                        "   \"amount\":5000," +
                        "   \"type\":\"cars\"" +
                        "}";
        String url = "http://localhost:8080/transactionservice/transaction/10";
        ResponseEntity response = RestUtils.putExchange(restTemplate, url, requestBody);
        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());

        requestBody =
                "{" +
                        "   \"amount\":10000," +
                        "   \"type\":\"shopping\"," +
                        "   \"parent_id\":10" +
                        "}";
        url = "http://localhost:8080/transactionservice/transaction/11";
        response = RestUtils.putExchange(restTemplate, url, requestBody);
        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void types() throws IOException {
        log.info("@Test listOfTransactionsIdsForGivenType()...");

        Long[] expectedTranIds = {10L};
        ResponseEntity<Long[]> responseEntity = restTemplate.getForEntity("http://localhost:8080/transactionservice/types/cars", Long[].class);
        Long[] tranIds = responseEntity.getBody();
        Assert.assertArrayEquals(expectedTranIds, tranIds);
    }

    @Test
    public void sameTransactionTypes() {
        log.info("@Test sameTransactionTypes()...");

        String requestBody =
                "{" +
                        "   \"amount\":999998887," +
                        "   \"type\":\"hedgefund\"" +
                        "}";
        String url = "http://localhost:8080/transactionservice/transaction/12";
        ResponseEntity response = RestUtils.putExchange(restTemplate, url, requestBody);
        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());

        requestBody =
                "{" +
                        "   \"amount\":-69998887," +
                        "   \"type\":\"hedgefund\"" +
                        "}";
        url = "http://localhost:8080/transactionservice/transaction/13";
        response = RestUtils.putExchange(restTemplate, url, requestBody);
        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());

        Long[] expectedTranIds = {12L, 13L};
        ResponseEntity<Long[]> responseEntity = restTemplate.getForEntity("http://localhost:8080/transactionservice/types/hedgefund", Long[].class);
        Long[] tranIds = responseEntity.getBody();
        Assert.assertArrayEquals(expectedTranIds, tranIds);
    }

    @Test
    public void sum10() throws IOException {
        log.info("@Test sum10()...");
        Double expectedSum = 15000d;
        ResponseEntity<Double> responseEntity = restTemplate.getForEntity("http://localhost:8080/transactionservice/sum/10", Double.class);
        RestUtils.checkJsonResponse(responseEntity, MediaType.APPLICATION_JSON, HttpStatus.OK);
        Double sumFor10 = responseEntity.getBody();
        Assert.assertEquals(expectedSum, sumFor10);
    }

    @Test
    public void sum11() {
        log.info("@Test sum11()...");
        Double expectedSum = 10000d;
        ResponseEntity<Double> responseEntity = restTemplate.getForEntity("http://localhost:8080/transactionservice/sum/11", Double.class);
        RestUtils.checkJsonResponse(responseEntity, MediaType.APPLICATION_JSON, HttpStatus.OK);
        Double sumFor11 = responseEntity.getBody();
        Assert.assertEquals(expectedSum, sumFor11);
    }


}


