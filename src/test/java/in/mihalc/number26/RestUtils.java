package in.mihalc.number26;

import org.junit.Assert;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Patrik Mihalčin
 * RestUtils is a helper class used in WebIntegrationTests
 */
public class RestUtils {

    public static ResponseEntity putExchange(RestTemplate restTemplate, String url, String requestBody) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(requestBody, headers);
        return restTemplate.exchange(url, HttpMethod.PUT, entity, String.class);
    }

    public static void checkJsonResponse(ResponseEntity responseEntity, MediaType expectedMediaType, HttpStatus expectedHttpStatus) {
        MediaType contentType = responseEntity.getHeaders().getContentType();
        HttpStatus statusCode = responseEntity.getStatusCode();

        // e.g. we can deal with application | JSON
        Assert.assertEquals(expectedMediaType.getType(), contentType.getType());
        Assert.assertEquals(expectedMediaType.getSubtype(), contentType.getSubtype());

        // e.g. OK(200, "OK")
        Assert.assertEquals(expectedHttpStatus, statusCode);
    }

}
