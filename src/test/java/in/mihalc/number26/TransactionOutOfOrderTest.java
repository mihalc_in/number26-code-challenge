package in.mihalc.number26;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

/**
 * Created by Patrik Mihalčin
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class TransactionOutOfOrderTest {
    private static final Logger log = LoggerFactory.getLogger(TransactionControllerTest.class);

    @Autowired
    private TransactionController transactionController;

    RestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void sendChildBeforeParent() throws IOException {
        log.info("@Test sendChildBeforeParent()...");

        transactionController.getSumByTranId().clear();
        transactionController.getTranIdsByType().clear();

        String requestBody =
                "{" +
                        "   \"amount\":10000," +
                        "   \"type\":\"shopping\"," +
                        "   \"parent_id\":10" +
                        "}";
        String url = "http://localhost:8080/transactionservice/transaction/11";
        ResponseEntity response = RestUtils.putExchange(restTemplate, url, requestBody);
        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());

        requestBody =
                "{" +
                        "   \"amount\":5000," +
                        "   \"type\":\"cars\"" +
                        "}";
        url = "http://localhost:8080/transactionservice/transaction/10";
        response = RestUtils.putExchange(restTemplate, url, requestBody);
        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }
}
