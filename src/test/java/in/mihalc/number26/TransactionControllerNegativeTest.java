package in.mihalc.number26;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

/**
 * Created by Patrik Mihalčin
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class TransactionControllerNegativeTest {
    private static final Logger log = LoggerFactory.getLogger(TransactionControllerTest.class);

    RestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void listOfTransactionsIdsForGivenTypeNegative() throws IOException {
        log.info("@Test listOfTransactionsIdsForGivenTypeNegative()...");
        restTemplate.setErrorHandler(new TransactionResponseErrorHandler());

        try {
            restTemplate.getForEntity("http://localhost:8080/transactionservice/types/cars", Long[].class);
        } catch (TransactionNotFoundException e) {
            String body = (String) e.getProperties().get("body");
            String code = (String) e.getProperties().get("code");
            Assert.assertTrue(body.contains("\"message\":\"No transactions of type 'cars' were found\",\"path\":\"/transactionservice/types/cars\""));
            Assert.assertEquals("404", code);
        }
    }

    @Test
    public void sum10Negative() throws IOException {
        log.info("@Test sum10Negative()...");

        restTemplate.setErrorHandler(new TransactionResponseErrorHandler());

        try {
            restTemplate.getForEntity("http://localhost:8080/transactionservice/sum/10", Double.class);
        } catch (TransactionNotFoundException e) {
            String body = (String) e.getProperties().get("body");
            String code = (String) e.getProperties().get("code");
            Assert.assertTrue(body.contains("\"message\":\"Transaction with transaction_id '10' was found\",\"path\":\"/transactionservice/sum/10\""));
            Assert.assertEquals("404", code);
        }
    }
}
